/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                                      
                      ███╗   ██╗    ███████╗    ██╗    ██╗    ███████╗
                      ████╗  ██║    ██╔════╝    ██║    ██║    ██╔════╝
    █████╗            ██╔██╗ ██║    █████╗      ██║ █╗ ██║    ███████╗
    ╚════╝            ██║╚██╗██║    ██╔══╝      ██║███╗██║    ╚════██║
                      ██║ ╚████║    ███████╗    ╚███╔███╔╝    ███████║
                      ╚═╝  ╚═══╝    ╚══════╝     ╚══╝╚══╝     ╚══════╝
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

paths['news'] = {
  'news': 'packages/bower/ionews/src/',
//  'news': src.vendors + 'dataview-intranetone-news/src/',
  'sortable': src.vendors + 'Sortable/',
  'moment': src.vendors + 'moment/',
  'moment_duration_format': src.vendors + 'moment-duration-format/lib/',
  'wickedpicker': src.vendors + 'wickedpicker/dist/',
  'dropzone': src.vendors + 'dropzone/dist/',
  'tinymce': src.vendors + 'tinymce/',
  'photoswipe': src.vendors + 'photoswipe/dist/',
}
//copy vendors
mix.copyDirectory(paths.news.tinymce,dest.io.vendors+'tinymce');
mix.copyDirectory(src.io.vendors + 'tinymce/moxiemanager/', dest.io.vendors + 'tinymce/plugins/moxiemanager/');

mix.copy(src.io.vendors + 'tinymce/pt_BR.js', dest.io.vendors + 'tinymce/langs/pt_BR.js');




mix.styles([
	src.base.css + 'helpers/dv-buttons.css',
	src.io.css + 'dropzone.css',
	src.io.css + 'dropzone-preview-template.css',
  src.io.vendors + 'aanjulena-bs-toggle-switch/aanjulena-bs-toggle-switch.css',
  src.io.css + 'sortable.css',
	paths.toastr + 'toastr.min.css',
	src.io.css + 'toastr.css',
  src.io.root + 'forms/video-form.css',
  paths.news.news + 'news.css',
], dest.io.services + 'io-news.min.css');

mix.babel([
	paths.news.sortable + 'Sortable.min.js',
  src.io.vendors + 'aanjulena-bs-toggle-switch/aanjulena-bs-toggle-switch.js',
	paths.toastr + 'toastr.min.js',
  paths.news.wickedpicker + 'wickedpicker.min.js',
	src.io.js + 'defaults/def-toastr.js',
	src.io.js + 'extensions/ext-pillbox.js',
  paths.news.dropzone + 'min/dropzone.min.js',
  src.io.js + 'dropzone-loader.js',
], dest.io.services + 'io-news-babel.min.js');

mix.scripts([
	paths.news.moment + 'min/moment.min.js',
	src.io.vendors + 'moment/moment-pt-br.js',
  paths.news.moment_duration_format +'moment-duration-format.js',
], dest.io.services + 'io-news-mix.min.js');
//copy separated for compatibility
mix.babel(paths.news.news + 'news.js', dest.io.services + 'io-news.min.js');
//mix.copy(, dest.vendors + 'dropzone/dropzone.min.js');
